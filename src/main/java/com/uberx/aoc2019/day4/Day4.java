package com.uberx.aoc2019.day4;

import com.google.common.base.Splitter;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Day4 {

    public static Pair<Integer, Integer> getRange(final String filename) throws IOException {
        final List<Integer> ints = Splitter.on('-')
                .splitToList(
                        IOUtils.toString(
                                Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                                StandardCharsets.UTF_8))
                .stream()
                .map(Integer::valueOf)
                .collect(Collectors.toList());
        return Pair.of(ints.get(0), ints.get(1));
    }

    public long getNumValidPasswords1(final int from, final int to) {
        return IntStream.rangeClosed(from, to)
                .filter(this::isPasswordValid1)
                .count();
    }

    public long getNumValidPasswords2(final int from, final int to) {
        return IntStream.rangeClosed(from, to)
                .filter(this::isPasswordValid2)
                .count();
    }

    private boolean isPasswordValid1(int password) {
        final Pair<Boolean, int[]> ascending = isAscending(password);
        return ascending.getLeft() && isValidDuplicate1(ascending.getRight());
    }

    private boolean isPasswordValid2(int password) {
        final Pair<Boolean, int[]> ascending = isAscending(password);
        return ascending.getLeft() && isValidDuplicate2(ascending.getRight());
    }

    private Pair<Boolean, int[]> isAscending(int password) {
        boolean descending = false;
        int[] duplicates = new int[10];
        int previousDigit = password % 10;
        int currentDigit;

        password /= 10;
        while (password > 0 && !descending) {
            currentDigit = password % 10;
            if (currentDigit > previousDigit) {
                descending = true;
            } else if (currentDigit == previousDigit) {
                duplicates[currentDigit]++;
            }
            password /= 10;
            previousDigit = currentDigit;
        }
        return Pair.of(!descending, duplicates);
    }

    private boolean isValidDuplicate1(final int[] duplicates) {
        return Arrays.stream(duplicates).anyMatch(duplicate -> duplicate > 0);
    }

    private boolean isValidDuplicate2(final int[] duplicates) {
        return Arrays.stream(duplicates).anyMatch(duplicate -> duplicate == 1);
    }
}
