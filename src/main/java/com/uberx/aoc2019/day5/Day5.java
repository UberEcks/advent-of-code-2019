package com.uberx.aoc2019.day5;

import com.google.common.base.Splitter;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.mutable.MutableLong;
import org.apache.commons.lang3.tuple.Pair;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Day5 {
    private final List<MutableLong> intCode;
    private final List<Long> outputs = new ArrayList<>();
    private int relativeBase = 0;
    private int currentIndex = 0;
    private int currentInputIndex = 0;
    private int currentOutputIndex = 0;
    private boolean halted;

    public Day5(final List<MutableLong> intCode) {
        this.intCode = intCode;
    }

    public static List<MutableLong> getIntCode(final String filename) throws IOException {
        return Splitter.on(',')
                .splitToList(
                        IOUtils.toString(
                                Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                                StandardCharsets.UTF_8))
                .stream()
                .map(Long::valueOf)
                .map(MutableLong::new)
                .collect(Collectors.toList());
    }

    public boolean hasNextOutput() {
        return currentOutputIndex < outputs.size();
    }

    public long getNextOutput() {
        return outputs.get(currentOutputIndex++);
    }

    public List<Long> getOutputs() {
        return outputs;
    }

    public boolean isRunning() {
        return !halted;
    }

    public void compute(final List<Long> inputs) {
        while (!halted) {
            final Instruction currentInstruction = Instruction.fromOpCode(
                    intCode.get(currentIndex).getValue().intValue());
            if (currentInstruction == Instruction.HALT) {
                halted = true;
                continue;
            }

            if (currentInstruction == Instruction.INPUT) {
                if (currentInputIndex >= inputs.size()) {
                    break;
                }
                final Pair<Long, Long> result = currentInstruction.execute(
                        intCode, relativeBase, currentIndex, inputs.get(currentInputIndex++));
                currentIndex = result.getLeft().intValue();
            } else {
                final Pair<Long, Long> result = currentInstruction.execute(intCode, relativeBase, currentIndex);
                currentIndex = result.getLeft().intValue();
                if (currentInstruction == Instruction.OUTPUT) {
                    this.outputs.add(result.getRight());
                } else if (currentInstruction == Instruction.RELATIVE_BASE_OFFSET) {
                    this.relativeBase += result.getRight();
                }
            }
        }
    }

    enum Instruction {
        ADD(1, 3),
        MULTIPLY(2, 3),
        INPUT(3, 1),
        OUTPUT(4, 1),
        JUMP_IF_TRUE(5, 2),
        JUMP_IF_FALSE(6, 2),
        LESS_THAN(7, 3),
        EQUALS(8, 3),
        RELATIVE_BASE_OFFSET(9, 1),
        HALT(99, 0);

        final int opcode;
        final int parameters;

        Instruction(final int opcode, final int parameters) {
            this.opcode = opcode;
            this.parameters = parameters;
        }

        static Instruction fromOpCode(final int opcode) {
            return Arrays.stream(Instruction.values())
                    .filter(i -> i.opcode == opcode % 100)
                    .findFirst()
                    .orElseThrow(() -> new IllegalStateException("Illegal opcode " + opcode));
        }

        public Pair<Long, Long> execute(final List<MutableLong> intCode,
                                        final int relativeBase,
                                        final int currentIndex,
                                        final long... input) {
            final int fullOpCode = intCode.get(currentIndex).getValue().intValue();
            final ParamMode param1Mode = ParamMode.fromMode(fullOpCode / 100 % 10);
            final ParamMode param2Mode = ParamMode.fromMode(fullOpCode / 1000 % 10);
            final ParamMode param3Mode = ParamMode.fromMode(fullOpCode / 10000 % 10);
            final MutableLong param1 = param1Mode.getMutableLong(intCode, currentIndex + 1, relativeBase);
            final long param1Value = param1.getValue();
            if (this.parameters >= 2) {
                final long param2Value = param2Mode.getMutableLong(intCode, currentIndex + 2, relativeBase).getValue();
                if (this.parameters == 3) {
                    final MutableLong param3 = param3Mode.getMutableLong(intCode, currentIndex + 3, relativeBase);
                    if (this == ADD) {
                        param3.setValue(param1Value + param2Value);
                    } else if (this == MULTIPLY) {
                        param3.setValue(param1Value * param2Value);
                    } else if (this == LESS_THAN) {
                        if (param1Value < param2Value) {
                            param3.setValue(1);
                        } else {
                            param3.setValue(0);
                        }
                    } else if (this == EQUALS) {
                        if (param1Value == param2Value) {
                            param3.setValue(1);
                        } else {
                            param3.setValue(0);
                        }
                    }
                } else {
                    if (this == JUMP_IF_TRUE) {
                        if (param1Value != 0) {
                            return Pair.of(param2Value, null);
                        }
                    } else if (this == JUMP_IF_FALSE) {
                        if (param1Value == 0) {
                            return Pair.of(param2Value, null);
                        }
                    }
                }
                return Pair.of(currentIndex + parameters + 1L, null);
            } else if (this == OUTPUT) {
                return Pair.of(currentIndex + parameters + 1L, param1Value);
            } else if (this == INPUT) {
                param1.setValue(input[0]);
                return Pair.of(currentIndex + parameters + 1L, null);
            } else if (this == RELATIVE_BASE_OFFSET) {
                return Pair.of(currentIndex + parameters + 1L, param1Value);
            } else {
                throw new IllegalStateException("Why am I here?");
            }
        }

        enum ParamMode {
            POSITION(0), IMMEDIATE(1), RELATIVE(2);

            final int mode;

            ParamMode(final int mode) {
                this.mode = mode;
            }

            static ParamMode fromMode(final int mode) {
                return Arrays.stream(ParamMode.values())
                        .filter(i -> i.mode == mode)
                        .findFirst()
                        .orElseThrow(() -> new IllegalStateException("Illegal mode " + mode));
            }

            public MutableLong getMutableLong(final List<MutableLong> intCode,
                                              final int index,
                                              final int relativeBase) {
                switch (this) {
                    case POSITION:
                        growIfNeeded(intCode, intCode.get(index).getValue().intValue());
                        return intCode.get(intCode.get(index).getValue().intValue());
                    case IMMEDIATE:
                        return intCode.get(index);
                    case RELATIVE:
                        growIfNeeded(intCode, intCode.get(index).getValue().intValue() + relativeBase);
                        return intCode.get(intCode.get(index).getValue().intValue() + relativeBase);
                    default:
                        throw new IllegalStateException("Why am I here?");
                }
            }

            private void growIfNeeded(final List<MutableLong> intCode,
                                      final int requiredIndex) {
                final int currentSize = intCode.size();
                if (requiredIndex >= currentSize) {
                    for (int i = 0; i < requiredIndex + 1 - currentSize; i++) {
                        intCode.add(new MutableLong(0));
                    }
                }
            }
        }
    }
}
