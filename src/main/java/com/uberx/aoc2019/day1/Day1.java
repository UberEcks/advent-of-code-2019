package com.uberx.aoc2019.day1;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Day1 {

    public static List<Integer> getInts(final String filename) throws IOException {
        return IOUtils
                .readLines(
                        Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                        StandardCharsets.UTF_8)
                .stream()
                .map(Integer::valueOf)
                .collect(Collectors.toList());
    }

    public int totalFuel1(final List<Integer> ints) {
        return ints.stream()
                .mapToInt(i -> i / 3 - 2)
                .sum();
    }

    public int totalFuel2(final List<Integer> ints) {
        return ints.stream()
                .mapToInt(i -> {
                    int total = 0;
                    while ((i = i / 3 - 2) >= 0) {
                        total += i;
                    }
                    return total;
                })
                .sum();
    }
}
