package com.uberx.aoc2019.day2;

import com.google.common.base.Splitter;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.mutable.MutableInt;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Day2 {
    private static final int ADD_INSTRUCTION = 1;
    private static final int MULTIPLY_INSTRUCTION = 2;
    private static final int HALT_INSTRUCTION = 99;

    public static List<MutableInt> getIntCode(final String filename) throws IOException {
        return Splitter.on(',')
                .splitToList(
                        IOUtils.toString(
                                Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                                StandardCharsets.UTF_8))
                .stream()
                .map(Integer::valueOf)
                .map(MutableInt::new)
                .collect(Collectors.toList());
    }

    public int compute(final List<MutableInt> intCode) {
        int currentIndex = 0;
        boolean halted = false;
        while (!halted) {
            final int currentInstruction = intCode.get(currentIndex).getValue();
            if (currentInstruction == HALT_INSTRUCTION) {
                halted = true;
                continue;
            }

            final int operand1 = intCode.get(intCode.get(currentIndex + 1).getValue()).getValue();
            final int operand2 = intCode.get(intCode.get(currentIndex + 2).getValue()).getValue();
            final int locationIndex = intCode.get(currentIndex + 3).getValue();
            final int result;
            if (currentInstruction == ADD_INSTRUCTION) {
                result = operand1 + operand2;
            } else if (currentInstruction == MULTIPLY_INSTRUCTION) {
                result = operand1 * operand2;
            } else {
                throw new IllegalStateException("Illegal instruction " + currentInstruction);
            }
            intCode.get(locationIndex).setValue(result);
            currentIndex += 4;
        }
        return intCode.get(0).getValue();
    }

    public int search(final String filename, final int expectedResult) throws IOException {
        for (int noun = 0; noun < 100; noun++) {
            for (int verb = 0; verb < 100; verb++) {
                final List<MutableInt> intCodeCopy = getIntCode(filename);
                intCodeCopy.get(1).setValue(noun);
                intCodeCopy.get(2).setValue(verb);
                final int result = this.compute(intCodeCopy);
                if (result == expectedResult) {
                    return 100 * noun + verb;
                }
            }
        }
        throw new IllegalStateException("Could not find any (noun, verb) that yields " + expectedResult);
    }
}
