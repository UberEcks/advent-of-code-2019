package com.uberx.aoc2019.day7;

import com.google.common.collect.Lists;
import com.uberx.aoc2019.day5.Day5;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

public class Day7 {

    public long findLargestOutputSignalWithLoop(final String filename,
                                                final Collection<List<Integer>> phaseSettings,
                                                final long initialInput) throws IOException {
        long largestOutput = Long.MIN_VALUE;

        for (List<Integer> phaseSetting : phaseSettings) {
            final Day5 ampA = new Day5(Day5.getIntCode(filename));
            final List<Long> ampAInputs = Lists.newArrayList(phaseSetting.get(0).longValue(), initialInput);

            final Day5 ampB = new Day5(Day5.getIntCode(filename));
            final List<Long> ampBInputs = Lists.newArrayList(phaseSetting.get(1).longValue());

            final Day5 ampC = new Day5(Day5.getIntCode(filename));
            final List<Long> ampCInputs = Lists.newArrayList(phaseSetting.get(2).longValue());

            final Day5 ampD = new Day5(Day5.getIntCode(filename));
            final List<Long> ampDInputs = Lists.newArrayList(phaseSetting.get(3).longValue());

            final Day5 ampE = new Day5(Day5.getIntCode(filename));
            final List<Long> ampEInputs = Lists.newArrayList(phaseSetting.get(4).longValue());

            while (ampA.isRunning() || ampB.isRunning() || ampC.isRunning() || ampD.isRunning() || ampE.isRunning()) {
                ampA.compute(ampAInputs);
                if (ampA.hasNextOutput()) {
                    ampBInputs.add(ampA.getNextOutput());
                }

                ampB.compute(ampBInputs);
                if (ampB.hasNextOutput()) {
                    ampCInputs.add(ampB.getNextOutput());
                }

                ampC.compute(ampCInputs);
                if (ampC.hasNextOutput()) {
                    ampDInputs.add(ampC.getNextOutput());
                }

                ampD.compute(ampDInputs);
                if (ampD.hasNextOutput()) {
                    ampEInputs.add(ampD.getNextOutput());
                }

                ampE.compute(ampEInputs);
                if (ampE.hasNextOutput()) {
                    ampAInputs.add(ampE.getNextOutput());
                }
            }
            largestOutput = Math.max(largestOutput, ampE.getOutputs().get(ampE.getOutputs().size() - 1));
        }
        return largestOutput;
    }
}
