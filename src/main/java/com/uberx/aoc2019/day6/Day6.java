package com.uberx.aoc2019.day6;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

public class Day6 {
    public static final String UNIVERSAL_CENTER_OF_MASS_OBJECT = "COM";

    public List<Pair<String, String>> getMapEntries(final String filename) throws IOException {
        return IOUtils
                .readLines(
                        Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                        StandardCharsets.UTF_8)
                .stream()
                .map(entry -> entry.split("\\)"))
                .map(objects -> Pair.of(objects[0], objects[1]))
                .collect(Collectors.toList());
    }

    public Map<String, Node> getObjects(final List<Pair<String, String>> mapEntries) {
        final Map<String, Node> objects = new HashMap<>();
        for (Pair<String, String> mapEntry : mapEntries) {
            final String objectName = mapEntry.getLeft();
            final String orbiterName = mapEntry.getRight();
            final Node current = objects.containsKey(objectName)
                    ? objects.get(objectName) : new Node(objectName);
            final Node orbiter = objects.containsKey(orbiterName)
                    ? objects.get(orbiterName) : new Node(orbiterName);

            objects.putIfAbsent(objectName, current);
            objects.putIfAbsent(orbiterName, orbiter);
            current.addOrbiter(orbiter);
            orbiter.parent = current;
        }
        objects.get(UNIVERSAL_CENTER_OF_MASS_OBJECT).depth = 0;
        return objects;
    }

    public int computeTotalOrbits(final Node com) {
        final Queue<Node> dfs = new LinkedList<>();
        dfs.offer(com);

        int orbits = 0;
        while (!dfs.isEmpty()) {
            final Node current = dfs.poll();
            orbits += current.depth;
            for (Node orbiter : current.orbiters) {
                orbiter.depth = current.depth + 1;
                dfs.offer(orbiter);
            }
        }
        return orbits;
    }

    public int computeMinOrbitalTransfers(final Node you,
                                          final Node san) {
        final Node lca = findLowestCommonAncestor(you, san);
        return (you.depth - lca.depth - 1) + (san.depth - lca.depth - 1);
    }

    private Node findLowestCommonAncestor(final Node you,
                                          final Node san) {
        Node youPointer = you;
        Node sanPointer = san;

        while (youPointer.depth > sanPointer.depth) {
            youPointer = youPointer.parent;
        }
        while (sanPointer.depth > youPointer.depth) {
            sanPointer = sanPointer.parent;
        }

        while (youPointer != sanPointer) {
            youPointer = youPointer.parent;
            sanPointer = sanPointer.parent;
        }
        return youPointer;
    }

    static class Node {
        String name;
        Node parent;
        List<Node> orbiters = new ArrayList<>();
        int depth = -1;

        public Node(final String name) {
            this.name = name;
        }

        public void addOrbiter(final Node orbiter) {
            this.orbiters.add(orbiter);
        }
    }
}
