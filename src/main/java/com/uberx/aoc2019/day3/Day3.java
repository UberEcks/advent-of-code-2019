package com.uberx.aoc2019.day3;

import com.google.common.base.Splitter;
import com.google.common.collect.Sets;
import lombok.AllArgsConstructor;
import lombok.ToString;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

public class Day3 {

    public static List<List<Path>> getAllPaths(final String filename) throws IOException {
        return IOUtils
                .readLines(
                        Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                        StandardCharsets.UTF_8)
                .stream()
                .map(paths -> Splitter.on(',')
                        .splitToList(paths)
                        .stream()
                        .map(path -> {
                            final Path.Direction direction = Path.Direction.valueOf(String.valueOf(path.charAt(0)));
                            final int length = Integer.parseInt(path.substring(1));
                            return new Path(direction, length);
                        })
                        .collect(Collectors.toList()))
                .collect(Collectors.toList());

    }

    public Map<Pair<Integer, Integer>, Integer> getOccupiedCoordinates(final List<Path> wire) {
        final Map<Pair<Integer, Integer>, Integer> occupiedCoordinates = new HashMap<>();
        final MutablePair<Integer, Integer> currentLocation = MutablePair.of(0, 0);
        int steps = 0;
        for (final Path path : wire) {
            if (path.direction == Path.Direction.L) {
                for (int column = currentLocation.right - 1;
                     column >= currentLocation.right - path.length;
                     column--) {
                    steps++;
                    occupiedCoordinates.putIfAbsent(Pair.of(currentLocation.left, column), steps);
                }
                currentLocation.right -= path.length;
            } else if (path.direction == Path.Direction.R) {
                for (int column = currentLocation.right + 1;
                     column <= currentLocation.right + path.length;
                     column++) {
                    steps++;
                    occupiedCoordinates.putIfAbsent(Pair.of(currentLocation.left, column), steps);
                }
                currentLocation.right += path.length;
            } else if (path.direction == Path.Direction.U) {
                for (int row = currentLocation.left - 1;
                     row >= currentLocation.left - path.length;
                     row--) {
                    steps++;
                    occupiedCoordinates.putIfAbsent(Pair.of(row, currentLocation.right), steps);
                }
                currentLocation.left -= path.length;
            } else if (path.direction == Path.Direction.D) {
                for (int row = currentLocation.left + 1;
                     row <= currentLocation.left + path.length;
                     row++) {
                    steps++;
                    occupiedCoordinates.putIfAbsent(Pair.of(row, currentLocation.right), steps);
                }
                currentLocation.left += path.length;
            }
        }
        return occupiedCoordinates;
    }

    public Set<Pair<Integer, Integer>> getIntersectionPoints(
            final List<Map<Pair<Integer, Integer>, Integer>> occupiedCoordinatesList) {
        Set<Pair<Integer, Integer>> intersectionPoints = occupiedCoordinatesList.get(0).keySet();
        for (final Map<Pair<Integer, Integer>, Integer> occupiedCoordinates :
                occupiedCoordinatesList.subList(1, occupiedCoordinatesList.size())) {
            intersectionPoints = Sets.intersection(intersectionPoints, occupiedCoordinates.keySet());
        }
        return intersectionPoints;
    }

    public int getManhattanDistanceOfClosestIntersection(final Set<Pair<Integer, Integer>> intersectionPoints) {
        int leastManhattanDistance = Integer.MAX_VALUE;
        for (Pair<Integer, Integer> intersectionPoint : intersectionPoints) {
            leastManhattanDistance = Math.min(
                    leastManhattanDistance, computeManhattanDistance(Pair.of(0, 0), intersectionPoint));
        }
        return leastManhattanDistance;
    }

    public int getLeastCombinedStepsToIntersection(
            final Set<Pair<Integer, Integer>> intersectionPoints,
            final List<Map<Pair<Integer, Integer>, Integer>> occupiedCoordinatesList) {
        int leastCombinedSteps = Integer.MAX_VALUE;
        for (Pair<Integer, Integer> intersectionPoint : intersectionPoints) {
            leastCombinedSteps = Math.min(
                    leastCombinedSteps,
                    occupiedCoordinatesList.stream()
                            .map(occupiedCoordinates -> occupiedCoordinates.get(intersectionPoint))
                            .mapToInt(Integer::intValue)
                            .sum());
        }
        return leastCombinedSteps;
    }

    private int computeManhattanDistance(final Pair<Integer, Integer> centralPortLocation,
                                         final Pair<Integer, Integer> intersectionLocation) {
        return Math.abs(centralPortLocation.getLeft() - intersectionLocation.getLeft())
                + Math.abs(centralPortLocation.getRight() - intersectionLocation.getRight());
    }

    @ToString
    @AllArgsConstructor
    static class Path {
        final Direction direction;
        final int length;

        enum Direction {
            L, R, U, D
        }
    }
}
