package com.uberx.aoc2019.day8;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Day8 {
    private static final int WIDTH_PIXELS = 25;
    private static final int HEIGHT_PIXELS = 6;
    private static final int TOTAL_PIXELS = WIDTH_PIXELS * HEIGHT_PIXELS;

    public static int[] getImageData(final String filename) throws IOException {
        final char[] chars = IOUtils.toCharArray(
                Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                StandardCharsets.US_ASCII);
        final int[] data = new int[chars.length];
        for (int i = 0; i < chars.length; i++) {
            data[i] = chars[i] - '0';
        }
        return data;
    }

    public int[] findLayerWithLeastZeroes(final List<int[]> layers) {
        int currentLeastZeroes = Integer.MAX_VALUE;
        int leastZeroesLayer = -1;
        for (int i = 0; i < layers.size(); i++) {
            if (layers.get(i)[0] < currentLeastZeroes) {
                currentLeastZeroes = layers.get(i)[0];
                leastZeroesLayer = i;
            }
        }
        return layers.get(leastZeroesLayer);
    }

    public List<int[]> getDigitCountsOfLayers(final int[] data) {
        final List<int[]> layers = new ArrayList<>();
        int[] digitCounts = new int[10];
        for (int i = 0; i < data.length; i++) {
            digitCounts[data[i]]++;
            if ((i + 1) % TOTAL_PIXELS == 0) {
                layers.add(digitCounts);
                digitCounts = new int[10];
            }
        }
        return layers;
    }

    public int[][] getDecodedImage(final int[] imageData) {
        final int[][] decodedImage = new int[HEIGHT_PIXELS][WIDTH_PIXELS];
        final int layers = imageData.length / TOTAL_PIXELS;
        for (int pixel = 0; pixel < TOTAL_PIXELS; pixel++) {
            final int imageRow = pixel / WIDTH_PIXELS;
            final int imageCol = pixel - (WIDTH_PIXELS * imageRow);
            for (int layer = 0; layer < layers; layer++) {
                final int currentIndex = (layer * TOTAL_PIXELS) + pixel;
                decodedImage[imageRow][imageCol] = imageData[currentIndex];
                if (imageData[currentIndex] == 0 || imageData[currentIndex] == 1) {
                    break;
                }
            }
        }
        return decodedImage;
    }

    public String printDecodedImage(final int[][] decodedImage) {
        final StringBuilder image = new StringBuilder();
        for (int[] ints : decodedImage) {
            for (int j = 0; j < decodedImage[0].length; j++) {
                if (ints[j] == 1) {
                    image.append("0 ");
                } else {
                    image.append("  ");
                }
            }
            image.append("\n");
        }
        return image.toString();
    }
}
