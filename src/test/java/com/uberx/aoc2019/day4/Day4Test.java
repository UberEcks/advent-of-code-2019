package com.uberx.aoc2019.day4;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class Day4Test {
    @Test
    public void testDay4() throws IOException {
        final Day4 solver = new Day4();
        final Pair<Integer, Integer> range = Day4.getRange("day4");
        assertEquals(1955, solver.getNumValidPasswords1(range.getLeft(), range.getRight()));
        assertEquals(1319, solver.getNumValidPasswords2(range.getLeft(), range.getRight()));
    }
}
