package com.uberx.aoc2019.day9;

import com.google.common.collect.Lists;
import com.uberx.aoc2019.day5.Day5;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class Day9Test {
    private static final long INPUT1 = 1;
    private static final long INPUT2 = 2;

    @Test
    public void testDay9() throws IOException {
        final Day5 solver1 = new Day5(Day5.getIntCode("day9"));
        solver1.compute(Lists.newArrayList(INPUT1));
        assertEquals(Lists.newArrayList(3409270027L), solver1.getOutputs());

        final Day5 solver2 = new Day5(Day5.getIntCode("day9"));
        solver2.compute(Lists.newArrayList(INPUT2));
        assertEquals(Lists.newArrayList(82760L), solver2.getOutputs());
    }
}
