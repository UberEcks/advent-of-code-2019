package com.uberx.aoc2019.day2;

import org.apache.commons.lang3.mutable.MutableInt;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class Day2Test {
    private static final int EXPECTED_RESULT = 19690720;

    @Test
    public void testDay2() throws IOException {
        final List<MutableInt> intCode = Day2.getIntCode("day2");

        final Day2 solver = new Day2();
        assertEquals(3654868, solver.compute(intCode));
        assertEquals(7014, solver.search("day2", EXPECTED_RESULT));
    }
}
