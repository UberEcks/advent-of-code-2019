package com.uberx.aoc2019.day1;

import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class Day1Test {
    @Test
    public void testDay1() throws IOException {
        final List<Integer> ints = Day1.getInts("day1");

        final Day1 solver = new Day1();
        assertEquals(3256599, solver.totalFuel1(ints));
        assertEquals(4882038, solver.totalFuel2(ints));
    }
}
