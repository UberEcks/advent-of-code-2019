package com.uberx.aoc2019.day6;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class Day6Test {
    private static final String YOU_OBJECT = "YOU";
    private static final String SAN_OBJECT = "SAN";

    @Test
    public void testDay6() throws IOException {
        final Day6 solver = new Day6();
        final List<Pair<String, String>> mapEntries = solver.getMapEntries("day6");
        final Map<String, Day6.Node> objects = solver.getObjects(mapEntries);

        final Day6.Node universalCenterOfMass = objects.get(Day6.UNIVERSAL_CENTER_OF_MASS_OBJECT);
        assertEquals(142497, solver.computeTotalOrbits(universalCenterOfMass));

        final Day6.Node you = objects.get(YOU_OBJECT);
        final Day6.Node san = objects.get(SAN_OBJECT);
        assertEquals(301, solver.computeMinOrbitalTransfers(you, san));
    }
}
