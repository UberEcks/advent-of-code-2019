package com.uberx.aoc2019.day3;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

public class Day3Test {
    @Test
    public void testDay3() throws IOException {
        final List<List<Day3.Path>> allPaths = Day3.getAllPaths("day3");

        final Day3 solver = new Day3();
        final List<Map<Pair<Integer, Integer>, Integer>> occupiedCoordinatesList = allPaths.stream()
                .map(solver::getOccupiedCoordinates)
                .collect(Collectors.toList());
        final Set<Pair<Integer, Integer>> intersectionPoints = solver.getIntersectionPoints(occupiedCoordinatesList);
        assertEquals(489, solver.getManhattanDistanceOfClosestIntersection(intersectionPoints));
        assertEquals(93654, solver.getLeastCombinedStepsToIntersection(intersectionPoints, occupiedCoordinatesList));
    }
}
