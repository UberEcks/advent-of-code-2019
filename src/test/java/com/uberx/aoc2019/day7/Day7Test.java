package com.uberx.aoc2019.day7;

import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class Day7Test {
    private static final long INITIAL_INPUT = 0;

    @Test
    public void testDay7() throws IOException {
        final Day7 solver = new Day7();
        assertEquals(
                34852,
                solver.findLargestOutputSignalWithLoop(
                        "day7",
                        Collections2.permutations(Lists.newArrayList(0, 1, 2, 3, 4)),
                        INITIAL_INPUT));
        assertEquals(
                44282086,
                solver.findLargestOutputSignalWithLoop(
                        "day7",
                        Collections2.permutations(Lists.newArrayList(5, 6, 7, 8, 9)),
                        INITIAL_INPUT));
    }
}
