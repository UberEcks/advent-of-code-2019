package com.uberx.aoc2019.day5;

import com.google.common.collect.Lists;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class Day5Test {
    private static final long INPUT_PART_1 = 1;
    private static final long INPUT_PART_2 = 5;

    @Test
    public void testDay5() throws IOException {
        final Day5 solver1 = new Day5(Day5.getIntCode("day5"));
        solver1.compute(Lists.newArrayList(INPUT_PART_1));
        assertEquals(Lists.newArrayList(0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 6731945L), solver1.getOutputs());

        final Day5 solver2 = new Day5(Day5.getIntCode("day5"));
        solver2.compute(Lists.newArrayList(INPUT_PART_2));
        assertEquals(Lists.newArrayList(9571668L), solver2.getOutputs());
    }
}
