package com.uberx.aoc2019.day8;

import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class Day8Test {
    private static final int[][] EXPECTED_DECODED_IMAGE = new int[][]{
            {1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0},
            {1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0},
            {1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0},
            {1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0},
            {1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0},
            {1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0}
    };

    @Test
    public void testDay8() throws IOException {
        final Day8 solver = new Day8();

        final int[] imageData = Day8.getImageData("day8");
        final List<int[]> digitsCountsOfLayers = solver.getDigitCountsOfLayers(imageData);
        final int[] layerWithLeastZeroes = solver.findLayerWithLeastZeroes(digitsCountsOfLayers);
        final int[][] decodedImage = solver.getDecodedImage(imageData);

        assertEquals(1848, layerWithLeastZeroes[1] * layerWithLeastZeroes[2]);
        System.out.println(solver.printDecodedImage(decodedImage));
        for (int i = 0; i < decodedImage.length; i++) {
            for (int j = 0; j < decodedImage[0].length; j++) {
                assertEquals(decodedImage[i][j], EXPECTED_DECODED_IMAGE[i][j]);
            }
        }
    }
}
